This is the Multivariate Normals (MVN) toolbox for Octave/Matlab.
http://www.ofai.at/~dominik.schnitzer/mvn
---


The MVN toolbox provides functions to handle multivariate Gaussians in Matlab.

It implements divergences for similarity computation:
 - The Kullback-Leibler divergence
 - The symmetrized Kullback-Leibler divergence
 - A Jensen-Shannon approximation
 - The Bhattacharyya coefficient
 - and othe functions to work with these divergences.
 
We also include a k-means clustering method for the multivariate Gaussians
and a method to compute native Self-Organizing maps for multivatiate Gaussians
and their divergences.

The author uses the toolbox for Music Similarity estimation and experiments
(as the features there are multivariate Gaussians), but its methods are of
course not limited to that topic.




August, 31th 2011
Dominik Schnitzer <dominik.schnitzer@ofai.at>
