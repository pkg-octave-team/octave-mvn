function [c] = mvn_bregmancentroid_kl_left(models)
%MVN_BREGMANCENTROID_KL_LEFT    Compute the left sided Kullback-Leibler (KL) centroid given mvn models. (GRADIENT SPACE CENTER OF MASS)
%
%   [c] = MVN_BREGMANCENTROID_KL_LEFT(models) computes the left sided KL
%   centroid for the given multivariate normals.

%   (c) 2010-2011, Dominik Schnitzer, <dominik.schnitzer@ofai.at>
%   http://www.ofai.at/~dominik.schnitzer/mvn
%
%   This file is part of the MVN Octave/Matlab Toolbox
%   MVN is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
%
%   MVN is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License
%   along with MVN.  If not, see <http://www.gnu.org/licenses/>.

    c.m = 0 .* models(1).m;

    % new means
    len = length(models);
    for i=1:len
        c.m = c.m + 1/len .* models(i).m;
    end

    % new covariance
    
%     % original:        
%     c.cov = c.m*c.m';
%     for i=1:len
%         c.cov = c.cov + 1/len .* (models(i).cov +...
%             models(i).m*models(i).m' - models(i).m*c.m' -...
%             c.m*models(i).m');

    c.cov = -1 .* c.m*c.m';
    for i=1:len
        c.cov = c.cov + 1/len.*(models(i).cov + models(i).m*models(i).m');
    end
    
    % compute inverse covariance
    
    % speedup for
    %
    %   c.logdet = log(det(c.cov));
    %   c.icov = inv(c.cov);
    %
    % using Cholesky:
    
    c_chol = chol(c.cov);
    c.logdet = 2*sum(log(diag(c_chol)));
    c_ui = c_chol\eye(length(c.m));
    c.icov = c_ui*c_ui';
end
