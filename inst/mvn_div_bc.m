function d = mvn_div_bc(m1, m2)
%MVN_DIV_BC Compute the Bhattacharyya coefficient between two
%   multivariate normals.
%
%   [d] = MVN_DIV_BC(m1, m2) computes the BC between two
%   multivariate normals. d is never negative.

%   (c) 2010-2011, Dominik Schnitzer, <dominik.schnitzer@ofai.at>
%   http://www.ofai.at/~dominik.schnitzer/mvn
%
%   This file is part of the MVN Octave/Matlab Toolbox
%   MVN is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
%
%   MVN is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License
%   along with MVN.  If not, see <http://www.gnu.org/licenses/>.

    mdiff = m1.m - m2.m;
    covsum = 0.5*(m1.cov + m2.cov);

%     % Original
%     d = 1/8 * ((mdiff' * inv(covsum)) * mdiff) +...
%         1/2*log(det(covsum)) - 1/4*m1.logdet - 1/4*m2.logdet;
    
    d = 1/8 * ((mdiff' / covsum) * mdiff) +...
        1/2*log(det(covsum)) - 1/4*m1.logdet - 1/4*m2.logdet;
    d = max(d, 0);
    
end
