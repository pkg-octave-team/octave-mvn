function [filenames, models_tc, models_oc] = mvn_test(testfile, t, tc_dim, oc_dim)
%MVN_TEST  Load a testcollection and prepare for using with the mvn functions.

%   (c) 2010-2011, Dominik Schnitzer, <dominik.schnitzer@ofai.at>
%   http://www.ofai.at/~dominik.schnitzer/mvn
%
%   This file is part of the MVN Octave/Matlab Toolbox
%   MVN is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
%
%   MVN is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License
%   along with MVN.  If not, see <http://www.gnu.org/licenses/>.

    data = load(testfile, 'data');
    if (strcmp(t, 'me') == 1)
        models_tc = struct('m', [], 'cov', [], 'logdet', [], 'icov', []);
        models_oc = [];
        type = 1;
    elseif (strcmp(t, 'ep') == 1)
        models_tc = struct('m', [], 'cov', [], 'logdet', [], 'icov', []);
        models_oc = struct('fp', [], 'fpg', [], 'fpb', []);
        type = 2;
    elseif (strcmp(t, 'mots4') == 1)
        models_tc = struct('m', [], 'cov', [], 'logdet', [], 'icov', []);
        models_oc = struct('fp', [], 'fpg', [], 'fpb', []);
        type = 3;
    elseif (strcmp(t, 'andre') == 1)
        models_tc =  struct('m', [], 'cov', [], 'logdet', [], 'icov', []);
        models_oc = struct('op', []);
        type = 4;
    else
        models_tc = struct('m', [], 'cov', [], 'logdet', [], 'icov', []);
        models_oc = struct('m', [], 'cov', [], 'logdet', [], 'icov', []);
        type = 0;
    end
    filenames = [];
    
    c = 1;
    ocd = 1:oc_dim;
    tcd = 1:tc_dim;
    
    for i = 1:length(data.data.cm)
        try
            
            if (type == 1)
                mtc = mvn_new(data.data.cm{i}.cov, data.data.cm{i}.mu);
                models_tc(c) = mtc;
            elseif (type == 2)
                mtc = mvn_new(data.data.cm{i}.cov, ...
                    data.data.cm{i}.m);
                mop.fp = data.data.cm{i}.fp;
                mop.fpg = data.data.cm{i}.fpg;
                mop.fpb = data.data.cm{i}.fp_bass;
                
                models_oc(c) = mop;
                models_tc(c) = mtc;
            elseif (type == 3)
                mtc = mvn_new(data.data.cm{i}.co, ...
                    data.data.cm{i}.m);
                mop.fp = data.data.cm{i}.fp;
                mop.fpg = data.data.cm{i}.fpg;
                mop.fpb = data.data.cm{i}.fpb;
                
                models_oc(c) = mop;
                models_tc(c) = mtc;
                
            elseif (type == 4)
                mtc = mvn_new(data.data.cm{i}.tc.cov(tcd, tcd), ...
                    data.data.cm{i}.tc.mu(tcd));
                models_oc(c).op = data.data.cm{i}.op;
                models_tc(c) = mtc;
            else
                mtc = mvn_new(data.data.cm{i}.tc.cov(tcd, tcd), ...
                    data.data.cm{i}.tc.mu(tcd));
                moc = mvn_new(data.data.cm{i}.oc.cov(ocd, ocd), ...
                    data.data.cm{i}.oc.mu(ocd));
                models_oc(c) = moc;
                models_tc(c) = mtc;
            end
            
            filenames{c} = data.data.filenames{i};
            
            c = c + 1;
        catch eobj
            %NYK, 2013: replaced the following block for Octave compatibility
            %if (strcmp(eobj.identifier, 'mvn:cov'))
            %    fprintf(2, 'Skipping model with invalid covariance: %d\n', i);
            %elseif (strcmp(eobj.identifier, 'MATLAB:posdef'))
            %    fprintf(2, ['Skipping model with non-positive definite ' ...
            %        'covariance: %d\n'], i);
            %else
            %    rethrow(eobj);
            %end
            fprintf(2, 'Skipping model with invalid covariance: %d\n', i);
        end
    end
    
end
