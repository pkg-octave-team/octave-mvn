function [cl_names, cl_assig] = mvn_fn2class(filenames, cl_pos, delim)
%MVN_FN2CLASS 

%   (c) 2010-2011, Dominik Schnitzer, <dominik.schnitzer@ofai.at>
%   http://www.ofai.at/~dominik.schnitzer/mvn
%
%   This file is part of the MVN Octave/Matlab Toolbox
%   MVN is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
%
%   MVN is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License
%   along with MVN.  If not, see <http://www.gnu.org/licenses/>.

    cl_names_all = cell(length(filenames), 1);

    for i = 1:length(filenames)
        fn = filenames{i};
        sp = strfind(fn, delim);
        cl_names_all{i} = fn(sp(cl_pos)+1:sp(cl_pos+1)-1);
    end

    cl_names = unique(cl_names_all);

    cl_assig = zeros(length(filenames), 1);
    for i = 1:length(filenames)
        cl_assig(i) = find(strcmp(cl_names, cl_names_all{i}) == 1);
    end
    
end
