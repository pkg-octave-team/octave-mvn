function [ ismetric ] = mvn_ismetric(d)
%MVN_ISMETRIC Analyzes the distance matrix D and returns the percentage of triples fulfilling the triangle inequality.
%
%   ATTENTION: Symmetry and Identity is presumed.
%
%   ismetric = MVN_ISMETRIC(d) Analyzes the given distance matrix and returns
%   the percentage of all possible triples fulfilling the triangle inequality.

%   (c) 2010-2011, Dominik Schnitzer, <dominik.schnitzer@ofai.at>
%   http://www.ofai.at/~dominik.schnitzer/mvn
%
%   This file is part of the MVN Octave/Matlab Toolbox
%   MVN is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
%
%   MVN is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License
%   along with MVN.  If not, see <http://www.gnu.org/licenses/>.

    ismetric = 0;
    for x = 1:length(d)
        ismetric = ismetric + sum((d(x,x) <= d(x,:) + d(x,:)));
        for y = (x+1):length(d)
            ismetric = ismetric + 2*sum( d(x, y) <= (d(x, :) + d(y, :)) );
        end
   end
    
   ismetric = ismetric / (length(d)^3);
   
end

