function d = mvn_div_skl(m1, m2)
%MVN_DIV_SKL  Compute the symmetric Kullback-Leibler (KL) divergence.
%
%   [d] = MVN_DIV_SKL(m1, m2) computes the SKL divergence between two
%   multivariate normals. d is never negative.
%
%   The SKL divergence is defined as:
%       d = 0.5*KL(m1, m2) + 0.5*KL(m2, m1)
%
%   Note: This function computes a faster version.

%   (c) 2010-2011, Dominik Schnitzer, <dominik.schnitzer@ofai.at>
%   http://www.ofai.at/~dominik.schnitzer/mvn
%
%   This file is part of the MVN Octave/Matlab Toolbox
%   MVN is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
%
%   MVN is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License
%   along with MVN.  If not, see <http://www.gnu.org/licenses/>.

    n = length(m1.m);
    m = m1.m - m2.m;

    d = (mvn_traceprod(m1.cov, m2.icov) + mvn_traceprod(m2.cov, m1.icov) + ...
         mvn_traceprod(m1.icov+m2.icov, m*m') - 2*n) / 4;

    d = max(d, 0);
end
