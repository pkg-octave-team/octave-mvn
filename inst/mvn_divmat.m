function [d] = mvn_divmat(models, t)
%MVN_DIVMAT  Compute a distance/divergence matrix using the specified MODELS and the given divergence T.
%
%   [d] = MVN_DIVMAT(models, t) Computes a divergence matrix using the given
%   mvn MODELS models and divergence T. T can be 'kl_left', 'kl_right', 
%   'skl', 'js', 'js_kl'. It defaults to the 'skl'.

%   (c) 2010-2011, Dominik Schnitzer, <dominik.schnitzer@ofai.at>
%   http://www.ofai.at/~dominik.schnitzer/mvn
%
%   This file is part of the MVN Octave/Matlab Toolbox
%   MVN is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
%
%   MVN is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License
%   along with MVN.  If not, see <http://www.gnu.org/licenses/>.

    if (nargin < 2)
        t = 'skl';
    end

    if (strcmp(t, 'kl_left') == 1)
        mvn_div = @(x, c) mvn_div_kl(x, c);
    elseif (strcmp(t, 'kl_right') == 1)
        mvn_div = @(x, c) mvn_div_kl(c, x);
    elseif (strcmp(t, 'skl') == 1)
        mvn_div = @(x, c) mvn_div_skl(x, c);
    elseif (strcmp(t, 'js') == 1)
        mvn_div = @(x, c) mvn_div_js(x, c);
    elseif (strcmp(t, 'js_kl') == 1)
        mvn_div = @(x, c) mvn_div_js(x, c, 1);
    end

    d = zeros(length(models), length(models), 'single');

    for i=1:length(models)
        if (isempty(models(i).m))
            continue;
        end
        for j=i+1:length(models)
            if (isempty(models(j).m))
                continue;
            end
            d(i, j) = mvn_div(models(i), models(j));
            d(j, i) = d(i, j);
        end
    end

end
