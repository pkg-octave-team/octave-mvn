function [c] = mvn_bregmancentroid_geodesic(d, c1, c2)
%MVN_BREGMANCENTROID_GEODESIC   Computes an arbitrary weighted KL centroid on the geodesic linking between two sided centroids c1 and c2.
%
%   [c] = MVN_BREGMANCENTROID_GEODESIC(d, m1, m2) computes the KL centroid
%   for the given sided centroids m1 and m2 weighting m1 with d and m2
%   with (1-d).
%
%   See: On the centroids of symmetrized bregman divergences, Nielsen, F.
%   and Nock, R., 2007.

%   (c) 2010-2011, Dominik Schnitzer, <dominik.schnitzer@ofai.at>
%   http://www.ofai.at/~dominik.schnitzer/mvn
%
%   This file is part of the MVN Octave/Matlab Toolbox
%   MVN is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
%
%   MVN is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License
%   along with MVN.  If not, see <http://www.gnu.org/licenses/>.

    c.m = d*c1.m + (1-d)*c2.m;
    c.cov = d*(c1.cov + c1.m*c1.m') + ...
        (1-d)*(c2.cov + c2.m*c2.m') - ...
        c.m*c.m';


    c_chol = chol(c.cov);
    c.logdet = 2*sum(log(diag(c_chol)));
    c_ui = c_chol\eye(length(c.m));
    c.icov = c_ui*c_ui';    
    
% Original version
% 
%     % compute gradient (gradF)
%     c1GF.m = c1.m;
%     c1GF.cov = -(c1.cov + c1.m*c1.m');
%     c2GF.m = c2.m;
%     c2GF.cov = -(c2.cov + c2.m*c2.m');
% 
%     % geodesic walk
%     gcGF.m = d*c1GF.m + (1-d)*c2GF.m;
%     gcGF.cov = d*c1GF.cov + (1-d)*c2GF.cov;
% 
%     % compute inverse gradient (gradF^-1)
%     c.m = gcGF.m;
%     c.cov = -(gcGF.cov + gcGF.m*gcGF.m');

end
