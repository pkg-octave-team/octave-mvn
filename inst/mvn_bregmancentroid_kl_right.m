function [c] = mvn_bregmancentroid_kl_right(models)
%MVN_BREGMANCENTROID_RIGHT    Compute the right centroid given mvn models ("center of mass").
%
%   [c] = MVN_BREGMANCENTROID_RIGHT(models) computes the right sided
%   centroid for the given multivariate normals.

%   (c) 2010, Dominik Schnitzer, <dominik.schnitzer@ofai.at>
%   http://www.ofai.at/~dominik.schnitzer/mvn
%
%   This file is part of the MVN Octave/Matlab Toolbox
%   MVN is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
%
%   MVN is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License
%   along with MVN.  If not, see <http://www.gnu.org/licenses/>.

    % compute the new mean in the natural parameter space
    len = length(models);
    c.m = 0 .* models(1).m;
    for i=1:len
        c.m = c.m + 1/len * (models(i).icov * models(i).m);
    end

    % compute the covariance in the natural parameter space
    c.cov = [];
    c.logdet = NaN;
    c.icov = 0 .* models(1).cov;
    
    for i=1:len
	    c.icov = c.icov + 1/len * models(i).icov;
    end

    % recover the the parameters of the multivariate gaussian distribution
    % from the natual parameters
    c.cov = inv(c.icov);
    c.m = c.cov * c.m;
    
    c_chol = chol(c.cov);
    c.logdet = 2*sum(log(diag(c_chol)));    
end
